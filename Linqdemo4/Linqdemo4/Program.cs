﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Linqdemo4
{
	class Program
	{

		public class Student
		{

			public int StudentID { get; set; }
			public string StudentName { get; set; }
		}
		static void Main(string[] args)
		{

			//use of Sequenceequal operator

			IList<string> collection1 = new List<string>() { "mukesh", "sanjay", "vikas" };
			IList<string> collection2 = new List<string>() { "mohit", "avneesh" };
			Student std = new Student() { StudentID = 1, StudentName = "mukesh" };

			IList<Student> studentList1 = new List<Student>() { std };

			IList<Student> studentList2 = new List<Student>() { std };

			bool result = studentList1.SequenceEqual(studentList2); // returns true

			Console.WriteLine(result);

			Student std1 = new Student() { StudentID = 1, StudentName = "sanjay" };

			Student std2 = new Student() { StudentID = 1, StudentName = "sanjay" };

			IList<Student> studentList3 = new List<Student>() { std1 };

			IList<Student> studentList4 = new List<Student>() { std2 };

			bool Result = studentList3.SequenceEqual(studentList4); // returns false

			Console.WriteLine(Result);

			//use of concat operator


			var concateResult = collection1.Concat(collection2);

			foreach (string str in concateResult)
				Console.WriteLine(str);

			Console.ReadKey();

		}
	}
}



