var selectedRow = null
function call() {
    var status = validate();
    if (status) {
        var formData = readFormData();
        if (selectedRow == null)
            insertNewRecord(formData);
        else
            updateRecord(formData);
        resetForm();
    }
}


function readFormData() {
    var formData = {};
    formData["fname"] = document.getElementById("fname").value;
    formData["mname"] = document.getElementById("mname").value;
    formData["lname"] = document.getElementById("lname").value;
    formData["email"] = document.getElementById("email").value;
    formData["mobileno"] = document.getElementById("mobileno").value;


    var gen = document.getElementsByName("gender");
    for (var i = 0; i < gen.length; i++) {
        if (gen[i].checked) {
            formData["gender"] = gen[i].value;

        }

    }

    formData["highereducation"] = document.getElementById("highereducation").value;
    
    var ch = document.getElementsByName("option");
    for (var i = 0; i < ch.length; i++) {
        if (ch[i].checked) {
            formData["option"] = ch[i].value;

        }
    }

    formData["address"] = document.getElementById("address").value;
    
   


    return formData;
}


function insertNewRecord(data) {
    var table = document.getElementById("show").getElementsByTagName('tbody')[0];
    var newRow = table.insertRow(table.length);
    cell1 = newRow.insertCell(0);
    cell2 = newRow.insertCell(1);
    cell3 = newRow.insertCell(2);
    cell4 = newRow.insertCell(3);
    cell5 = newRow.insertCell(4);
    cell6 = newRow.insertCell(5);
    cell7 = newRow.insertCell(6);
    cell8 = newRow.insertCell(7);
    cell9 = newRow.insertCell(8);
    cell10 = newRow.insertCell(9);

    cell1.innerHTML = data.fname;
    cell2.innerHTML = data.mname;
    cell3.innerHTML = data.lname;
    cell4.innerHTML = data.email;
    cell5.innerHTML = data.mobileno;
    cell6.innerHTML = data.gender;
    cell7.innerHTML = data.highereducation;
    cell8.innerHTML = data.option;
    cell9.innerHTML = data.address;
    cell9.innerHTML = data.address;
    cell9.innerHTML = data.address;
    cell10.innerHTML = `<button type=button class="btn btn-success" onClick="onEdit(this)">Edit</button><br> <button type=button class="btn btn-danger" onClick="onDelete(this)">Delete</button>`;
}



function resetForm() {
  
    
    document.getElementById("fname").value = "";
    document.getElementById("mname").value = "";
    document.getElementById("lname").value = "";
    document.getElementById("email").value = "";
    document.getElementById("mobileno").value = "";

    var gen = document.getElementsByName("gender");
    for (var i = 0; i < gen.length; i++) {
        if (gen[i].checked) {
            gen[i].checked=false;

        }

    }
    
    document.getElementById("highereducation").value = "";
    var ch = document.getElementsByName("option");
    for (var i = 0; i < ch.length; i++) {
        if (ch[i].checked) {
             ch[i].checked=false;

        }
    }
    document.getElementById("address").value = "";
    selectedRow = null;
}


function onEdit(td) 
{
    selectedRow = td.parentElement.parentElement;
    document.getElementById("fname").value = selectedRow.cells[0].innerHTML;
    document.getElementById("mname").value = selectedRow.cells[1].innerHTML;
    document.getElementById("lname").value = selectedRow.cells[2].innerHTML;
    document.getElementById("email").value = selectedRow.cells[3].innerHTML;
    document.getElementById("mobileno").value = selectedRow.cells[4].innerHTML;
   // document.getElementByName("gender").value = selectedRow.cells[5].innerHTML;
    var gen = document.getElementsByName("gender");
    for (var i = 0; i < gen.length; i++) {
        if (gen[i].value==selectedRow.cells[5].innerHTML) {
            document.getElementById(gen[i].id).checked=true;

        }
    }
    
    document.getElementById("highereducation").value = selectedRow.cells[6].innerHTML;
    //document.getElementById("option").value = selectedRow.cells[7].innerHTML;
    var opt = document.getElementsByName("option");
    for (var i = 0; i < opt.length; i++) {
        if (opt[i].value==selectedRow.cells[7].innerHTML) {
            document.getElementById(opt[i].id).checked=true;

        }
    }

    document.getElementById("address").value = selectedRow.cells[8].innerHTML;
}


function updateRecord(formData)
{
    selectedRow.cells[0].innerHTML=formData.fname;
    selectedRow.cells[1].innerHTML=formData.mname;
    selectedRow.cells[2].innerHTML=formData.lname;
    selectedRow.cells[3].innerHTML=formData.email;
    selectedRow.cells[4].innerHTML=formData.mobileno;
    selectedRow.cells[5].innerHTML=formData.gender;
    selectedRow.cells[6].innerHTML=formData.highereducation;
    selectedRow.cells[7].innerHTML=formData.option;
    selectedRow.cells[8].innerHTML=formData.address;
    
}

function onDelete(td) {
        row = td.parentElement.parentElement;
        document.getElementById("show").deleteRow(row.rowIndex);
        resetForm();
    }




function validate() {
    var status = true;


    var fname = $('#fname').val();
    var mname = $('#mname').val();
    var lname = $('#lname').val();
    var email = $('#email').val();
    var mobileno = $('#mobileno').val();
    var education = $('#highereducation').val();
    var gender = $('input[name="gender"]:checked');
    var address = $('#address').val();


    if (fname == "") {

        $('label[name=first_name]').html("First Name:");
       // $('label[name=first_name]').css("color", "red");
        $('#errorfname').html("**First name must be filled.");
        $('#errorfname').css("color", "red");
        status = false;

    }
    else if (!isNaN(fname)) {
        $('label[name=first_name]').html("First Name:");
        $('#errorfname').html("**only alphabate must be allowed.");
    }
   else if ((fname.length <= 2) || (fname.length >= 26)) {
        $('label[name=first_name]').html("First Name:");
        $('#errorfname').html("**First name must be between 2 and 26 character .");
    }

    else {

        $('label[name=first_name]').html("First Name:");
        $('label[name=first_name]').css("color", "black");
        $('#errorfname').html("");


    }

    // Middle Name
   


    // Last Name

    if (lname == "") {

        $('label[name=last_name]').html("Last Name:");
       // $('label[name=first_name]').css("color", "red");
        $('#errorlname').html("**Last name must be filled.");
        $('#errorlname').css("color", "red");
        status = false;

    }
    else if (!isNaN(lname)) {
        $('label[name=last_name]').html("Last Name:");
        $('#errorlname').html("**only alphabate must be allowed.");
    }
   else if ((lname.length <= 2) || (lname.length >= 26)) {
        $('label[name=last_name]').html("Last Name:");
        $('#errorlname').html("**Last name must be between 2 and 26 character .");
    }

    else {

        $('label[name=last_name]').html("Last Name:");
        $('label[name=last_name]').css("color", "black");
        $('#errorlname').html("");


    }

    // Email validation

    if (email == "") {
        $('label[name=email_add]').html("Email:");
       // $('label[name=email_add]').css("color", "red");
        $('#erroremail').html("**Email must be filled.");
        $('#erroremail').css("color", "red");


        status = false;


    }
    else if ((email.indexOf('@') <= 0) || (email.charAt(email.length - 4) != '.')){
        $('label[name=email_add]').html("Email:");
        // $('label[name=email_add]').css("color", "red");
         $('#erroremail').html("** Enter a valid Email id");
         $('#erroremail').css("color", "red");

    }

    else {
        $('label[name=email_add]').html("Email:");
        $('label[name=email_add]').css("color", "black");
        $('#erroremail').html("");

    }



    // Mobile Number validation


    if (mobileno == "")  {
        $('label[name=mobile_no]').html("Mobile Number:");
      //  $('label[name=mobile_no]').css("color", "red")
        $('#errormobileno').html(" **Mobile number must be filled.");
        $('#errormobileno').css("color", "red");

        status = false;


    }
    else if ((mobileno.length > 0 && mobileno.length < 10) || (mobileno.length > 10)) {
        $('label[name=mobile_no]').html("Mobile Number:");
        //  $('label[name=mobile_no]').css("color", "red")
          $('#errormobileno').html("**Mobile number must be only 10 digit.");
          $('#errormobileno').css("color", "red");
    }
    else if (isNaN(mobileno)) {
        $('label[name=mobile_no]').html("Mobile Number:");
        //  $('label[name=mobile_no]').css("color", "red")
          $('#errormobileno').html("**only numeric value is allowed.");
          $('#errormobileno').css("color", "red");
    }

    else {
        $('label[name=mobile_no]').html("Mobile Number:");
        $('label[name=mobile_no]').css("color", "black")
        $('#errormobileno').html("");

    }



    // Gender validation

    // if(gender=="")
    // {
    //     $('label[name=g]').html("Gender*");
    //     $('label[name=g]').css("color", "red");
    //     status=false;

    // }
    // else
    // {
    //     $('label[name=g]').html("Gender:");
    //     $('label[name=g]').css("color", "black");
    // }






    // Highereducation


    if (education == "") {
        $('label[name=course]').html("Higher Education:");
       // $('label[name=course]').css("color", "red");
        status = false;
    }
    else {
        $('label[name=course]').html("Higher Education");
        $('label[name=course]').css("color", "black");

    }


    if (address == "") {

        $('label[for=address]').html("Address:");
       // $('label[for=address]').css("color", "red");
       $('#erroraddress').html("**Address must be filled.")
        $('#erroraddress').css("color", "red");

        status = false;
    }
    else {

        $('label[for=address]').html("Address:");
        $('label[for=address]').css("color", "black");
        $('#erroraddress').html("");

    }
    return status;
}
