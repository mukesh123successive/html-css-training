﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebApplication2.Models;


namespace WebApplication2.Controllers
{
    public class StudentController : Controller
    {

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]

        public ActionResult Login(Loginview model)
        {
            if (FormsAuthentication.Authenticate(model.Username, model.Password))
            {
                FormsAuthentication.SetAuthCookie(model.Username, false);
                return RedirectToAction("Welcome", "Home");

            }
            ModelState.AddModelError("", "Invalid credentials");
            return View();

        }
            public RedirectToRouteResult Logout()
            {
                FormsAuthentication.SignOut();
               return RedirectToAction("Login");
            }
        }
    
}