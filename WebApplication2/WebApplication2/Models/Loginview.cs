﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace WebApplication2.Models
{
    public class Loginview
    {
        [Required(ErrorMessage = "Username is Blank")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Password is Blank")]
        public string Password{ get; set; }
    }
}