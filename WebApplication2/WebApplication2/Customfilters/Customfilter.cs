﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Diagnostics;
using System.Web.Mvc;
using WebApplication2.Customfilters;
using System.Web.Routing;

namespace WebApplication2.Customfilters
{
    public class Customfilter : FilterAttribute, IActionFilter
    {
        Stopwatch watch;
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            watch.Stop();
            filterContext.HttpContext.Response.Write("Action Execuation time is" + watch.ElapsedMilliseconds.ToString());
        }

       

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            watch = Stopwatch.StartNew();
        }

       
    }

     
    }

    
