﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Linqdemo6
{

    public class Student
    {

        public int StudentID { get; set; }
        public string StudentName { get; set; }
        public int Age { get; set; }
    }
    class Program
    {
        static void Main(string[] args)
        {
            IList<Student> studentList = new List<Student>() {
            new Student() { StudentID = 1, StudentName = "Mukesh", Age = 13 } ,
            new Student() { StudentID = 2, StudentName = "Sanjay",  Age = 15 } ,
            new Student() { StudentID = 3, StudentName = "vikas",  Age = 18 } ,
            new Student() { StudentID = 4, StudentName = "Rohit" , Age = 12 } ,
            new Student() { StudentID = 5, StudentName = "Mayank" , Age = 21 }
        };
            //use of let keyword in LINQ
            var result = from s in studentList
                         let Result = s.StudentName.ToLower()
                         where Result.StartsWith("m")
                         select Result;

            //use of into keyword in LINQ

            Console.WriteLine("print studentname whose id is greater than 2");
            var result1 = from s in studentList
                         where s.StudentID > 2
                         select s
                         into Result1
                         where Result1.StudentName.StartsWith("M")
                         select Result1;


            foreach (Student std in result1)
            {
                Console.WriteLine(std.StudentName);

                foreach (var name in result)
                    Console.WriteLine(name);
                Console.ReadKey();
            }
        }
    }
}
    
