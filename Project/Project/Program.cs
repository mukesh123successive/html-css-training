﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project
{
    class Program
    {
        static void Main(string[] args)
       {
            string input;
            int a;

            Task1 t1 = new Task1();
            Task2 t2 = new Task2();
            Task3 t3 = new Task3();
            Task4 t4 = new Task4();



           do
           {
                Console.WriteLine("Enter your choice to run task");
                Console.WriteLine("1.Press1 to sort numbers ");
                Console.WriteLine("2.Press2 to display unique numbers");
                Console.WriteLine("3.Press3 to find smallest 3 number");
                Console.WriteLine("4.Press4 to count number of vowels");

               Console.Write("Enter Your Choice :");

                input = Console.ReadLine();
                Int32.TryParse(input, out int choice);
                switch (choice)
               {
                    case 1:
                       t1.Program1();
                       break;

                   case 2:
                        t2.program2();
                        break;

                    case 3:
                       t3.program3();
                       break;

                    case 4:
                        t4.program4();
                        break;

                    default:
                        Console.WriteLine("Invalid Choice");
                        break;

               }
         Console.WriteLine();
         Console.WriteLine(" To continue press 1 ");
          a = Convert.ToInt32(Console.ReadLine());
            } while (a == 1) ;

         Console.ReadLine();

       }
    }
}


class Task1 {


    public void Program1()
    {
        var number = new int[5];
       // Console.WriteLine("First task is running");
        Console.WriteLine("Enter 5 unique numbers");

        for (int i = 0; i < 5; i++)
        {
            while (true)
            {
                var newValue = Convert.ToInt32(Console.ReadLine());
                var currentNumber = Array.IndexOf(number, newValue);
                if (currentNumber == -1)
                {
                    number[i] = newValue;
                    break;
                }
                Console.WriteLine("you already entered that number. retry.");
            }
        }

        Array.Sort(number);
        Console.WriteLine();

        foreach (var n in number)
            Console.WriteLine(n);

        Console.ReadLine();
    }
}

class Task2 {

    public void program2()
    {
        var list = new List<int>();

        while (true)
        {
            // Console.WriteLine("second task is running");
            Console.WriteLine("Enter a number or Quit to leave");
            var input = Console.ReadLine();

            if (input.CompareTo("Quit") == 0)
                break;
            else
            {
                var number = Convert.ToInt32(input);
                if (list.Contains(number))
                    continue;
                else
                    list.Add(number);
            }
        }

        foreach (var output in list)
            Console.Write("{0} ", output);
    }
}

class Task3 {
    public void program3()
    {
        var list = new List<int>();
        while (true)
        {
            Console.WriteLine("Enter a list of  five comma seperated number");
            var input = Console.ReadLine();

            var array = input.Split(',');

            if ((array.Length == 0) || (array.Length < 5))
            {
                Console.WriteLine("Invalid list! Try again.");
            }
            else
            {
                foreach (var number in array)
                    list.Add(Convert.ToInt32(number));
                break;
            }
        }
        list.Sort();

        for (int i = 0; i < 3; i++)
            Console.Write("{0} ", list[i]);
    }
}

//public static void task4(string str)
//{
//    try
//    {

//        int vowelCount = 0;
//        for (int i = 0; i < str.Length; i++)
//        {
//            if (str[i] == 'a' || str[i] == 'e' || str[i] == 'i' || str[i] == 'o' || str[i] == 'u' || str[i] == 'A' || str[i] == 'E' || str[i] == 'I' || str[i] == 'O' || str[i] == 'U')
//            {
//                vowelCount++;
//            }
//        }

//        Console.WriteLine("Your String '" + str + "' contains  '" + vowelCount + "' Vowels in it");
//    }
//    catch (Exception ex)
//    {
//        Console.WriteLine("error= " + ex);
//    }
//}


   class Task4
      {
     public void program4()
        {
        Console.Write("Enter String:");
        string str = Console.ReadLine();
        int vowelCount = 0;
         for (int i = 0; i < str.Length; i++)
            {
          if (str[i] == 'a' || str[i] == 'e' || str[i] == 'i' || str[i] == 'o' || str[i] == 'u' || str[i] == 'A' || str[i] == 'E' || str[i] == 'I' || str[i] == 'O' || str[i] == 'U')
                                {
            vowelCount++;
                }
                }
        
        Console.WriteLine("Total No of Vowel is: " + vowelCount);
         }
    
        }









