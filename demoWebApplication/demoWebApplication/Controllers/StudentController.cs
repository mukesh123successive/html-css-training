﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace demoWebApplication.Controllers
{
    public class Student
    {

        public int StudentID { get; set; }
        public string StudentName { get; set; }
        public int Age { get; set; }



    }
    public class StudentController : Controller
    {

        IList<Student> studentList = new List<Student>() {
                    new Student(){ StudentID=1, StudentName="Sanjay", Age = 21 },
                    new Student(){ StudentID=2, StudentName="mukesh", Age = 25 },
                    new Student(){ StudentID=3, StudentName="avneesh", Age = 20 },
                    new Student(){ StudentID=4, StudentName="vikas", Age = 31 },
                    new Student(){ StudentID=5, StudentName="satyam", Age = 19 }
                };
        // GET: Student
        public ActionResult Students()
        {
            //viewbag
            ViewBag.TotalStudents = studentList.Count();

            return View();
        }
        public ViewResult viewdatastudent()
        {
            ViewData["students"] = new List<string>()
         {
             "mukesh",
             "sanjay",
             "satyam",
             "anubhav"
            };
            return View();
        }

        public ActionResult tempdatastudent()
        {
            TempData["name"] = "madhur";

            return View();
        }

        public ActionResult About()
        {
            string name;

            if (TempData.ContainsKey("name"))
                name = TempData["name"].ToString();

            return View();
        }

        public ActionResult Contact()
        {
         

            return View();
        }
    }
}
    
