﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace demoWebApplication.Controllers
{
    [RoutePrefix("std")]
    public class homeController : Controller
    {
      
        // GET: home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Student()
        {
            return View();
        }
        [Route("student")]
        public string Getstudent()
        {
            return "All student";
        }
        //Route with Constraint
        [Route("student/{Id:int:min(2):max(10)}")]
        public string Getstudent(int Id)
        {
            return "student" + Id;
        }

        //default value in attribute routing
        [Route("student/Name/{Name=mukesh}")]
        public string Getstudent(string Name)
        {
            return "student" + Name;
        }
    }
}
