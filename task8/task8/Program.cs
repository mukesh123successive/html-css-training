﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task8
{
    enum month
    {

        
        january,
        febuary,
        march,
        april,
        may,
        june,
        july,
        august,
        september,
        october,
        november,
        december

    }
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string , int> monthlist =
                       new Dictionary<string, int>();

            // Adding key/value pairs  
            // in the Dictionary 
            // Using Add() method 
            monthlist.Add("january",20);
            monthlist.Add("february", 21);
            monthlist.Add("march", 22);
            monthlist.Add("april", 24);
            monthlist.Add("may", 28);
            monthlist.Add("june", 27);
            monthlist.Add("july", 26);
            monthlist.Add("august", 25);
            monthlist.Add("september", 30);
            monthlist.Add("october", 20);
            monthlist.Add("november", 31);
            monthlist.Add("december", 10);


            foreach (KeyValuePair<string, int> month in monthlist)
            {
                //string s = Enum.GetName(typeof(month),0);
                //Console.WriteLine("{0} and {1}",
                //          s, month.Value);
                Console.WriteLine("{0} and {1}",
                        month.Key, month.Value);
                // string s = Enum.GetName(typeof(month), 0);
                // Console.WriteLine("{0} and {1}",
                // s, month.Value);
                //string s = Enum.GetName(typeof(month), 0);
                //  Console.WriteLine("{0} and {1}",
                // s, month.Value);

            }
            Console.ReadKey();
        }
    }
}
