﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CrudWebApplication1.Models
{
    public class Employee
    {
        [Required]
        public int Sr_no { get; set; } = 0;

        [Required]
        public string Emp_name { get; set; } = "";

        [Required]
        public string City { get; set; } = "";

        [Required]
        public string State { get; set; } = "";

        [Required]
        public string Country { get; set; } = "";

        [Required]
        public string Department { get; set; } = "";

        [Required]
        public string flag { get; set; } = "";
    }
}
