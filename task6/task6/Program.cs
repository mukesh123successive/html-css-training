﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task5
{

     // creating abstract class
      abstract class Vehicle
    {
        
       
       // creating abstract method  
        abstract public int calculatetollamount();
    }
    //    public Vehicle(string color, int noofwheels, int maxspeed)
    //    {
    //        colors = color;
    //        wheels = noofwheels;

    //    }
    //    public string colors
    //    {
    //        get
    //        {
    //            return color;
    //        }
    //        set
    //        {
    //            color = value;
    //        }
    //    }
    //    public int wheels
    //    {
    //        get
    //        {
    //            return noofwheels;
    //        }
    //        set
    //        {
    //            noofwheels = value;
    //        }
    //    }
    //}

    class Car : Vehicle
    {
        private string name;
        public string color;
        public int noofwheels;
        public const int maxspeed = 100;
        public int tollamont = 200;

        // constructor 
        public Car(string c, int w, int s)
        {
            c = color;
            w = noofwheels;
            s = maxspeed;

        }
       
        // override method from abstract class
        public override int calculatetollamount()
        {
            return tollamont;
        }


        public void DisplaycarData()
        {
            Console.WriteLine("The Color of car is " + color);
            Console.WriteLine("The no wheels in car" + noofwheels);
            Console.WriteLine("The maxspeed of car is " + maxspeed);
        }

    }
    class Bike : Vehicle
    {
        private string name;
        public string color;
        public int noofwheels;
        public const int maxspeed = 100;
        public int tollamont = 200;

        // constructor 
        public Bike(string c, int w, int s)
        {
            c = color;
            w = noofwheels;
            s = maxspeed;

        }
         
        // override method
        public override int calculatetollamount()
        {
            return tollamont;
        }


        public void DisplayBikeData()
        {
            Console.WriteLine("The Color of bike is " + color);
            Console.WriteLine("The no wheels in bike" + noofwheels);
            Console.WriteLine("The maxspeed of bike is " + maxspeed);
        }

    }


    class Program
    {
        public static void Main()
        {
            // Create and initialize the  
            // object of AreaOfTank 
            Car c = new Car("Brown", 4, 1051);
            c.DisplaycarData();
            Console.WriteLine("tollamount of car is" + c.calculatetollamount());

            Bike b = new Bike("Blue", 4, 1051);
            b.DisplayBikeData();
            Console.WriteLine("tollamount of bike is" + c.calculatetollamount());
            Console.ReadKey();
            


        }
    }
}



