﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace linqdemo2
{

    class Student
    {

        // properties rollNo and name 
        public string name
        {
            get;
            set;
        }

        public int rollno
        {
            get;
            set;
        }
        public int age
        {
            get;
            set;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            List<Student> stddetails = new List<Student>() {
            new Student{ name = "mukesh", rollno = 10, age=24 },
                new Student{ name = "Sanajy", rollno=11, age=25},
                new Student{ name = "vikas", rollno=12, age=25 },
                new Student{ name = "mohit", rollno=13, age=27 },
                new Student {name = "satyam", rollno=14, age=28 }
        };
            //use of orderby query operator

            //var details = stddetails.OrderBy(x => x.name);

            //foreach (var value in details)
            //{
            //    Console.WriteLine(value.name + " " + value.rollno);

            //}

            //use of thenby query operator
           // var Result = stddetails.OrderBy(s => s.name).ThenBy(s => s.age);

            //   var thenByDescResult = stddetails.OrderBy(s => s.name).ThenByDescending(s => s.age);



            //foreach (var std in Result)
            //    Console.WriteLine("Name: {0}, Age:{1}", std.name, std.age);


            //use of groupby operator
            var Result = stddetails.GroupBy(s => s.age);

            foreach (var ageGroup in Result)
            {
                Console.WriteLine("Age Group: {0}", ageGroup.Key);

                foreach (Student s in ageGroup)
                    Console.WriteLine("Student Name: {0}", s.name);
            }

            Console.ReadKey();
        }
    }
}
