﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2
{
    class Program
    {
        static void Main(string[] args)
        {
            int a;
            Console.WriteLine("Enter value");
            string str = Console.ReadLine();

            Console.WriteLine("1.int");
            Console.WriteLine("2.float");
            Console.WriteLine("3.double");



            do
            {
                Console.WriteLine("Enter your choice");
                int m;
                m = Convert.ToInt32(Console.ReadLine());

                switch (m)
                {
                    case 1:
                        strToInt(str);
                        break;

                    case 2:
                        strToFloat(str);
                        break;

                    case 3:
                        strToDouble(str);
                        break;

                    default:
                        Console.WriteLine("Invalid choice :");
                        Console.WriteLine("Please Enter Valid Choice ");
                        break;

                }

                Console.WriteLine("To continue press 1 key ");
                
                a = Convert.ToInt32(Console.ReadLine());


            } while (a == 1);

            Console.ReadKey();

        }

        public static void strToInt(string str)
        {
            int result;
            bool b1;
            b1 = int.TryParse(str, out result);
            if (b1)
            {
                Console.WriteLine("Integer value is: " + result);

            }
            else
            {
                Console.WriteLine("Conversion does not exit.");


            }
        }
        public static void strToFloat(string str)
        {
            int result;
            bool b1;
            b1 = int.TryParse(str, out result);
            if (b1)
            {
                Console.WriteLine("Float value is: " + result);

            }
            else
            {
                Console.WriteLine("Conversion does not exit.");


            }
        }
        public static void strToDouble(string str)
        {
            int result;
            bool b1;
            b1 = int.TryParse(str, out result);
            if (b1)
            {
                Console.WriteLine("double value is: " + result);

            }
            else
            {
                Console.WriteLine("Conversion does not exit.");


            }
        }
    }
    }

