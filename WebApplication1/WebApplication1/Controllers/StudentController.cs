﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class StudentController : Controller
    {
        // GET: Student
        public ActionResult Login()
        {
            return View();
        }
        public ActionResult create()
        {
            return View();
        }

        //primitive model binding using primitive datatype string
        /* [HttpPost]
         public ContentResult Login(string studentName, string password)
         {
             if(studentName=="mukesh" && password == "mukesh@123")
             {
                 return Content("welcome mukesh in student section");
             }
             else
             {
                 return Content("sorry" + studentName);
             }*/

        //complex model binding using model user and content result return type

        [HttpPost]
        public ContentResult Login(user model )
        {
            if (model.studentName == "mukesh" && model.Password == "mukesh@123")
            {
                return Content("welcome mukesh in student section");
            }
            else
            {
                return Content("sorry" + model.studentName);
            }
        }
    }
}
