﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {

            List<Employee> emp = new List<Employee>();
            Employee employee = new Employee();

            emp.Add(new Employee { EmployeeID = 1, EmployeeName = "mukesh", Department = "dotnet" });

            emp.Add(new Employee { EmployeeID = 2, EmployeeName = "sanjay", Department = "R&D" });

            emp.Add(new Employee { EmployeeID = 3, EmployeeName = "vikas", Department = "Machinelearning" });

            emp.Add(new Employee { EmployeeID = 4, EmployeeName = "anubhav", Department = "Java" });

            emp.Add(new Employee { EmployeeID = 5, EmployeeName = "avneesh", Department = "Cloud" });

            emp.Add(new Employee { EmployeeID = 6, EmployeeName = "kanika", Department = "Azure" });

            return View(emp);
        }
        public JsonResult Index1()
        {
            return Json(new { Name = "Zain Ul Hassan", ID = 1 }, JsonRequestBehavior.AllowGet);
        }


    }
}