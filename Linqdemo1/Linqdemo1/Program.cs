﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Linqdemo1
{

    public class Employee
    {

        public int emp_id
        {
            get;
            set;
        }
        public string emp_name
        {
            get;
            set;
        }
        public string emp_gender
        {
            get;
            set;
        }
        public int emp_age
        {
            get;
            set;
        }
        public int emp_salary
        {
            get;
            set;
        }
    }


    class Program
    {
        static void Main(string[] args)
        {
            ArrayList myarray = new ArrayList();
            myarray.Add(new Employee() { emp_id = 101, emp_name = "mukesh", emp_gender = "male"});
            myarray.Add(new Employee() { emp_id = 102, emp_name = "sanjay", emp_gender = "male" });
            myarray.Add(new Employee() { emp_id = 103, emp_name = "vikas", emp_gender = "male" });

            
            // Using OfType operator 
            var res = myarray.OfType<Employee>();

            foreach (var a in res)
            {
                Console.WriteLine("Employee Id: {0}", a.emp_id);
                Console.WriteLine("Employee Id: {0}", a.emp_name);
                Console.WriteLine("Employee Id: {0}", a.emp_gender);

                //Console.ReadKey();
            }
            Console.ReadKey();
        }
    }
    }

