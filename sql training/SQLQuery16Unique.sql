CREATE TABLE customer (
    CustomerID int NOT NULL,
    FirstName varchar(255) NOT NULL,
    LastName varchar(255),
	Country varchar(255),
	City varchar(255)
    
    CONSTRAINT UC_Person UNIQUE (CustomerID,LastName)
);