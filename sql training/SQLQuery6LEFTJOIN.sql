--LEFT OUTER JOIN METHOD

SELECT Orders.OrderID, Customers.CustomerName, Orders.OrderDate, Customers.Country
FROM Orders
LEFT JOIN Customers
ON Orders.CustomerID=Customers.CustomerID
ORDER BY Orders.OrderID
