--Defaultconstarint
CREATE TABLE ConstraintDemo
(
        ID INT PRIMARY KEY,
	    Name VARCHAR(50) NULL,
	    EmployeeDate DATETIME NOT NULL DEFAULT GETDATE()
)
GO

INSERT INTO ConstraintDemo ([ID],[NAME],EmployeeDate) VALUES (1,'mukesh','2016/10/22')
INSERT INTO ConstraintDemo ([ID],[NAME]) VALUES (2,'sanajy')
INSERT INTO ConstraintDemo ([ID],[NAME],EmployeeDate) VALUES (3,'rohan','2020/12/10')
INSERT INTO ConstraintDemo ([ID],[NAME]) VALUES (4,'vikas')
INSERT INTO ConstraintDemo ([ID],[NAME],EmployeeDate) VALUES (5,'rohan','2020/12/19')

SELECT * FROM ConstraintDemo