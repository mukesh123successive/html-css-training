--RIGHT JOIN METHOD

SELECT Orders.OrderID, Customers.CustomerName, Orders.OrderDate, Customers.Country
FROM Orders
RIGHT JOIN Customers
ON Customers.CustomerID=Orders.CustomerID
