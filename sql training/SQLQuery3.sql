--CREATE DATABASE Datatype

-- Numeric data types

--DECLARE @Datatypeint INT = 2
--PRINT @Datatypeint

--DECLARE @Datatypeint tinyINT = 8
--PRINT @Datatypeint

--DECLARE @Datatypeint bigINT = 3456789
--PRINT @Datatypeint

--DECLARE @DatatypeDecimal DECIMAL (3,2) = 2.3189
--PRINT @DatatypeDecimal

-- string datatypes

--DECLARE @Datatypechar  VARCHAR(30) = 'My name is Mukesh Gupta'
--PRINT @Datatypechar

--DECLARE @Datatypechar  CHAR = 'My name is Mukesh Gupta'
--PRINT @Datatypechar

-- DATE time datatypes

--DECLARE @DatatypeDate DATE = '2020-12-08'
--PRINT @DatatypeDate

--DECLARE @Datatype_Date DATETIME = '08-12-2020'
--PRINT @Datatype_Date

--DECLARE @DatatypeDate DATETIME2 = '2020-12-08'
--PRINT @DatatypeDate

--DECLARE @DatatypeDATE TIME = '2030-01-01'
--PRINT @DatatypeDATE

-- use of  UNIQUEIDENTIFIER datatype

--SELECT NEWID()
--DECLARE @UNItype UNIQUEIDENTIFIER
--SET @UNItype = NEWID()
 
--SELECT @UNItype

--CONVERSION IN DIFFERENT DATE FORMAT

--Select SYSDATETIME() as [SYSDATETIME]

--Select GETDATE() as [GETDATE]

--declare @date datetime
--Set @date=GETDATE()
--Select CONVERT(varchar,@date,1) as [MM/DD/YY]

--declare @date datetime
--Set @date=GETDATE()
--Select CONVERT(varchar,@date,1) as [MM/DD/YY]

--declare @date datetime
--Set @date=GETDATE()
--Select CONVERT(varchar,@date) as [DD MMM YY]

--Select CONVERT(varchar,@date) as [hh:mm:ss]
--Select CONVERT(varchar,@date) as [mm/dd/yy hh:mm:ss (AM/PM)]

--DATE FUNCTIONS

--DATEDIFF FUNCTION

SELECT DATEDIFF(year, '2020/08/25', '2025/08/25') AS DateDiff;
SELECT DATEDIFF(month, '2020/08/25', '2025/08/25') AS DATEDiff;

SELECT DATEDIFF(HOUR, '2017/08/25', '2011/08/25') AS DateDiff
SELECT DATEDIFF(week, '2017/08/25', '2011/08/25') AS DateDiff;

SELECT DATEDIFF(DAY, '2017/08/25', '2011/08/25') AS DateDiff;
SELECT DATEDIFF(second, '2020/08/25', '2025/08/25') AS DateDiff;

--DATEADD  FUNCTION
SELECT DATEADD(month, 2, '2020/12/08') AS DateAdd;
SELECT DATEADD(year, 5, '2017/08/25') AS DateAdd;
SELECT DATEADD(week, 2, '2017/08/25') AS DateAdd;

--String datatype function

--CONCAT FUNCTION

--SELECT CONCAT('MY', '','NAME','IS','MUKESH');

--LEN FUNCTION

--SELECT LEN('MUKESHGUPTA');

--LTRIM FUNCTION

--SELECT LTRIM('    MUKESHGUPTA') As LeftTrimmedString;
--SELECT RTRIM('MUKESHGUPTA      ') As RightTrimmedString;

--NCHAR FUNCTION
--SELECT NCHAR(65) AS NumberCodeToUnicode
--SELECT NCHAR(69) AS NumberCodeToUnicode
--SELECT NCHAR(75) AS NumberCodeToUnicode

--REPLICATE FUNCTION

--SELECT REPLICATE('MUKESH',5);

--Translate function
--SELECT TRANSLATE('Mukesh', 'Mu', 'gu');

--DATATYPE CONVERSION IN SQL SERVER

--int datatype

DECLARE @datatypeint INT=24
SELECT CAST(@datatypeint as datetime);

DECLARE @datatypefloat INT=24
SELECT CAST(@datatypefloat as float);

DECLARE @datatypebit INT=24
SELECT CAST(@datatypebit as bit);

DECLARE @datatypevarchar INT=24
SELECT CAST(@datatypevarchar as varchar);

DECLARE @datatypenvarchar INT=24
SELECT CAST(@datatypenvarchar as nvarchar);

DECLARE @datatypebigint bigINT=2434567
SELECT CAST(@datatypebigint as datetime);

SELECT CONVERT(decimal, 25.65987);

--datetype conversion

DECLARE @datatypedatetoint datetime='2020-12-08'
SELECT CAST(@datatypedatetoint as int);

DECLARE @datatypedatetovar datetime='2020-12-08'
SELECT CAST(@datatypedatetovar as varchar);

DECLARE @datatypedatetobit datetime='2020-12-08'
SELECT CAST(@datatypedatetobit as bit);

DECLARE @datatypedatetofloat datetime='2020-12-08'
SELECT CAST(@datatypedatetofloat as float);

--Float conversion

DECLARE @datatypefloattoint float=34.98
SELECT CAST(@datatypefloattoint as int);

DECLARE @datatypefloattodatetime float=34.98
SELECT CAST(@datatypefloattodatetime as datetime);

DECLARE @datatypefloattochar float=34.98
SELECT CAST(@datatypefloattochar as varchar);
SELECT @datatypefloattochar + CAST(1000 As varchar(10));

--char conversion
DECLARE @datatypechartoint varchar='5678934'
SELECT CAST(@datatypechartoint as int);

DECLARE @datatypechartodatetime varchar='5678934'
SELECT CAST(@datatypechartodatetime as datetime);






























