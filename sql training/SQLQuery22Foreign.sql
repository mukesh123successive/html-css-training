CREATE TABLE Person1 (
      PersonID INT PRIMARY KEY,
	  FirstName varchar(255) NOT NULL,
	  LastName varchar(255) NOT NULL,
	  Country varchar(255),
	  City varchar(255),
	  Age int
)
GO
CREATE TABLE Order1 (
    OrderID int NOT NULL,
    OrderNumber int NOT NULL,
    --PersonID int,
    --PRIMARY KEY (OrderID),
    PersonID INT FOREIGN KEY REFERENCES Person1(PersonID)
)


INSERT INTO Person1([PersonID],[FirstNAME],[LastNAME],[Country],[City],[Age]) VALUES (101, 'mukesh', 'gupta','India', 'Delhi', 25)
INSERT INTO Person1([PersonID],[FirstNAME],[LastNAME],[Country],[City],[Age]) VALUES (102, 'rohit', 'kumar','usa', 'Delhi', 26)
INSERT INTO Person1([PersonID],[FirstNAME],[LastNAME],[Country],[City],[Age]) VALUES (103, 'sanjay', 'gupta','london', 'Delhi', 27)
INSERT INTO Person1([PersonID],[FirstNAME],[LastNAME],[Country],[City],[Age]) VALUES (104, 'vikas', 'gupta','france', 'Delhi', 28)
INSERT INTO Person1([PersonID],[FirstNAME],[LastNAME],[Country],[City],[Age]) VALUES (105, 'satyam', 'gupta','australia', 'Delhi', 29)
INSERT INTO Person1([PersonID],[FirstNAME],[LastNAME],[Country],[City],[Age]) VALUES (106, 'mohit', 'gupta','japan', 'Delhi', 30)

SELECT * FROM Person1

INSERT INTO Order1([OrderID],[OrderNumber],[PersonID]) VALUES (5, 241567,101)
INSERT INTO Order1([OrderID],[OrderNumber],[PersonID]) VALUES (6, 241568,102)
INSERT INTO Order1([OrderID],[OrderNumber],[PersonID]) VALUES (7, 241569,103)
INSERT INTO Order1([OrderID],[OrderNumber],[PersonID]) VALUES (8, 241570,103)
INSERT INTO Order1([OrderID],[OrderNumber],[PersonID]) VALUES (9, 241571,104)
INSERT INTO Order1([OrderID],[OrderNumber],[PersonID]) VALUES (10, 241572,105)

SELECT * FROM Order1



