--Different keywords in SQL

SELECT * FROM student
WHERE FirstName='suresh';

SELECT * FROM student
ORDER BY Age;

SELECT COUNT(ID), FirstName
FROM student
GROUP BY FirstName;

SELECT COUNT(ID), Age
FROM student
GROUP BY Age
HAVING COUNT(ID) > 3

SELECT * FROM student
WHERE FirstName LIKE 'm%';

SELECT * FROM student
WHERE FirstName IN ('mukesh');

