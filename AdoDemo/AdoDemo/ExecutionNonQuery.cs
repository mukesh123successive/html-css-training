﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace AdoDemo
{
    class ExecutionNonQuery
    {
        public void NonQuery()
        {
            using (SqlConnection con = new SqlConnection("Data Source=LAPTOP-SBKC7E4J\\MUKESHSQLEXPRESS;Initial Catalog=student;User Id=sa;Password=Mukesh"))
            {

                // Insert a record in database
                SqlCommand cmd = new SqlCommand("INSERT INTO Employee VALUES (107,'satish','garg','india','punjab',31)", con);
                //SqlCommand cmd = new SqlCommand("INSERT INTO Employee VALUES (108,'satish','garg','india','punjab',31)", con);

                //Delete a record from database
                //SqlCommand cmd = new SqlCommand("DELETE FROM Employee WHERE EMPID=106", con);

                //Retrive data

               // SqlCommand cmd = new SqlCommand("Select * from student", con);
                con.Open();

                int TotalRowAffected = cmd.ExecuteNonQuery();
               // Console.WriteLine("Total Row Inserted = " + TotalRowAffected);
                Console.WriteLine("Total Row deleted = " + TotalRowAffected);

            }
        }
    }
}
