﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace AdoDemo
{
    public class ExecuteScalarMethod
    {
        public void Scalar()
        {
            //create connection to connect database
            string cs = "Data Source=LAPTOP-SBKC7E4J\\MUKESHSQLEXPRESS;Initial Catalog=student;User Id=sa;Password=Mukesh";

            using (SqlConnection con = new SqlConnection(cs))
            {
                //writing query to retrive data from database
                SqlCommand cmd = new SqlCommand("select count(EMPID) from Employee", con);
                cmd.Connection = con;

                //open connection
                con.Open();

                //Execuate query
                object totalRows = cmd.ExecuteScalar();
                Console.WriteLine("totalRows= " + totalRows);


            }
        }
    }
}
