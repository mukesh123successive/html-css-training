﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace AdoDemo
{
    class ExecuteReaderMethod
    {
        public void Reader()
        {
           

            using (SqlConnection con = new SqlConnection("Data Source=LAPTOP-SBKC7E4J\\MUKESHSQLEXPRESS;Initial Catalog=student;User Id=sa;Password=Mukesh"))
            {
                SqlCommand cmd = new SqlCommand("Select * from Employee", con);
                con.Open();
                //Call ExecuteReader to return a DataReader 
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    Console.WriteLine(rdr["FirstName"].ToString() + rdr["LastName"]);
                }
                //Release resources
                rdr.Close();
               


            }
        }
    }
}