﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Humanizer;

namespace task11
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(TimeSpan.FromMilliseconds(1299630020).Humanize(3));
            Console.WriteLine(TimeSpan.FromMilliseconds(8568908909).Humanize(3));
            Console.ReadKey();
        }
    }
}
