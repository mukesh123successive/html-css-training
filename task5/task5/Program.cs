﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task5
{
    class Vehicle
    {
        private string name;
        public string color;
        public int noofwheels;
        public const int maxspeed = 100;
        public int tollamont = 200;

        public Vehicle(string color, int noofwheels, int maxspeed)
        {
            colors = color;
            wheels = noofwheels;

        }
        public string colors
        {
            get
            {
                return color;
            }
            set
            {
                color = value;
            }
        }
        public int wheels
        {
            get
            {
                return noofwheels;
            }
            set
            {
                noofwheels = value;
            }
        }
    }

    class Car : Vehicle
    {
        int modelnumber;
        public Car(string color, int noofwheels, int m) : base( color,noofwheels,maxspeed)
        {
            modelnumber = m;
        }

        public double calculatetollamount()
        {
            // Console.WriteLine("tollamount is" + tollamont);
            return tollamont;
        }
        public void DisplaycarData()
        {
            Console.WriteLine("The Color of car is " + color);
            Console.WriteLine("The no wheels in car" + noofwheels);
            Console.WriteLine("The model number of car is " + modelnumber);
        }

        class Bike : Vehicle
        {

            public Bike(string color, int noofwheels, int maxspeed) : base(color, noofwheels, maxspeed)
            {

            }

            public double calculatetollamount()
            {
                //Console.WriteLine("tollamount is" + tollamont);
                return tollamont;
            }
            public void DisplayBikedata()
            {
                Console.WriteLine("The Color of bike is " + color);
                Console.WriteLine("The no wheels" + noofwheels);
                Console.WriteLine("The speed of bike is" + maxspeed);

            }


        }
        static void Main()
        {
            // Create and initialize the  
            // object of AreaOfTank 
            Car c = new Car("Brown", 4, 1051);
            c.DisplaycarData();
            Console.WriteLine("tollamount of car is" + c.calculatetollamount());
            

            Bike b = new Bike("blue", 2, 150);
            b.DisplayBikedata();
            Console.WriteLine("tollamountof bike is" + c.calculatetollamount());
            Console.ReadKey();




        }
    }
    }

