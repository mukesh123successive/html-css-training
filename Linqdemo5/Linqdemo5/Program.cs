﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Linqdemo5
{
  
        public class Employee
        {

            public int emp_id
            {
                get;
                set;
            }

            public string emp_name
            {
                get;
                set;
            }

            public string emp_gender
            {
                get;
                set;
            }

            public int emp_salary
            {
                get;
                set;
            }
        }
    class Program
    {
        static void Main(string[] args)
        {
            List<Employee> emp = new List<Employee>() {

            new Employee() {emp_id = 209, emp_name = "mukesh", emp_gender = "male", emp_salary = 20000},

            new Employee() {emp_id = 210, emp_name = "Sanjay", emp_gender = "male",emp_salary = 30000},

            new Employee() {emp_id = 211, emp_name = "Rohit", emp_gender = "Male",emp_salary = 40000},

            new Employee() {emp_id = 212, emp_name = "vikas", emp_gender = "male",emp_salary = 80000},

            new Employee() {emp_id = 213, emp_name = "Avneesh", emp_gender = "Male", emp_salary = 60000},

            new Employee() {emp_id = 214, emp_name = "anubhav", emp_gender = "male",emp_salary = 50000},
        };

            // Using Skip method 

            Console.WriteLine("skip initial three record");
            var result = (from e in emp
                       select e.emp_id)
                   .Skip(3);

            foreach (var a in result)
            {
                Console.WriteLine("Employee Id: {0}", a);
            }

            //use of skipwhile operator
            var res = emp.SkipWhile(e => e.emp_salary>40000);

            foreach (var val in res)
            {
                Console.WriteLine("Employee Name: {0}",val.emp_name);
            }

            //use of take operator
            Console.WriteLine("Print intial 4 employee");

            var result1 = (from e in emp
                       select e.emp_name)
                 .Take(4);

            foreach (var val in result1)
            {
                Console.WriteLine("Employee Name: {0}", val);
            }

            //use of takewhile operator
            Console.WriteLine("print employee whose salary is less than 50000");
            var result2 = emp.TakeWhile(e => e.emp_salary < 50000);

            foreach (var val in result2)
            {
                Console.WriteLine("Employee Name: {0}",
                                         val.emp_name);
            }


            //use of range and repeat operator
            // var Collection1 = Enumerable.Range(10, 10);
            //var Collection2 = Enumerable.Repeat<int>(10, 10);



            // for (int i = 0; i < Collection2.Count(); i++)
            // Console.WriteLine("Value at index {0} : {1}", i, Collection1.ElementAt(i));
            // Console.WriteLine("Value at index {0} : {1}", i, Collection2.ElementAt(i));





            Console.ReadKey();

        }
    }
}
