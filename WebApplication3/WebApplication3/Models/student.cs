﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace WebApplication3.Models
{
    public class student
    {
        //Perform client side validation 
        [Required]
        public int Id { get; set; }

        [Required]
        [Display(Name ="UserName")]
        [StringLength(20, ErrorMessage ="username contain not contain more than 20 characters ")]
        public string Name { get; set; }

        [Required]
        public string Password { get; set; }

        //compare validation
        [Required]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }

        //Range validation
        [Required]
        [Range(18,31, ErrorMessage ="Please enter age between 18 to 31")]
        public int Age { get; set; }

        //Regular expression validation
        [Required]
        [RegularExpression(".+\\@.+\\..+", ErrorMessage ="Please enter valid emailid")]
        public string Email { get; set; }
    }
}