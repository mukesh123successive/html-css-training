﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task9
{
    public interface IPainter
    {
        void Paint();
    }

    public interface SeatCover
    {
        string ChangeSeatCover();
    }

    public class Vehicle : IPainter   //Class having attributes, methods and constructors.
    {
        private string name;
        public string color;
        public int noofwheels;
        public const int maxspeed = 100;
        public int tollamont = 200;
        private string _PaintColor;
        public enum noOfWheels
        {
            car = 4,
            Bike = 2,
            Truck = 4
        }
        const int maxSpeed = 60;   //assigning constant (fixed) value 

        public Vehicle(string color, int noofwheels, int maxspeed)
        {
            this.color = color;
            this.noofwheels = noofwheels;

        }

        public string Colors
        {
            get;
            set;
        }


        public int Wheels
        {
            get;
            set;
        }
        public string PaintColor
        {
            get { return _PaintColor; }
            set { _PaintColor = value; }
        }
        

        public void Paint()
        {
            Console.WriteLine("This is paint method");
        }


        public void Start()
        {
            Console.WriteLine("Vehicle is running");
        }
        public void Stop()
        {
            Console.WriteLine("Vehicle stopped");
        }
        class Car : Vehicle,SeatCover
        {
            int modelnumber;
            //private int wheels;
            public Car(string color, int noofwheels, int m) : base(color, noofwheels, maxspeed)
            {
                modelnumber = m;
            }

            public double calculatetollamount()
            {
                // Console.WriteLine("tollamount is" + tollamont);
                return tollamont;
            }
            public void DisplaycarData()
            {
                Console.WriteLine("The Color of car is " + color);
                Console.WriteLine("The no wheels in car" + noofwheels);
                Console.WriteLine("The model number of car is " + modelnumber);
            }
            public string ChangeSeatCover()
            {
                return base.PaintColor = "Brown";
            }


            class Bike : Vehicle,SeatCover
            {

                public Bike(string color, int noofwheels, int maxspeed) : base(color, noofwheels, maxspeed)
                {

                }

                public double calculatetollamount()
                {
                    //Console.WriteLine("tollamount is" + tollamont);
                    return tollamont;
                }
                public void DisplayBikedata()
                {
                    Console.WriteLine("The Color of bike is " + color);
                    Console.WriteLine("The no wheels" + noofwheels);
                    Console.WriteLine("The speed of bike is" + maxspeed);

                }
                public string ChangeSeatCover()
                {
                    return base.PaintColor = "Blue";
                }


            }
            static void Main()
            {
                // Create and initialize the  
                // object of AreaOfTank 
                Car c = new Car("Brown", 4, 1051);
                c.DisplaycarData();
                Console.WriteLine("tollamount of car is" + c.calculatetollamount());
                Console.WriteLine("Seat Color :" + c.ChangeSeatCover());


                Bike b = new Bike("blue", 2, 150);
                b.DisplayBikedata();
                Console.WriteLine("tollamountof bike is" + c.calculatetollamount());
                //Console.WriteLine("Seat Color :" + b.ChangeSeatCover());

                Console.ReadKey();




            }
        }
    }
}

       