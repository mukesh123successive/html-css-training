﻿using crudwebapplication.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace crudwebapplication.Service
{
    public class EmployeeServices
    {
        string connect = "Data Source=LAPTOP-SBKC7E4J\\MUKESHSQLEXPRESS;Initial Catalog = Testdb; User Id = sa; Password=Mukesh;";
        //public string connect = ConfigurationManager.ConnectionStrings["myconn"].ConnectionString;
        private SqlDataAdapter _adapter;
        private DataSet _ds;


    public IList<EmployeeModel> GetEmployeeList()
        {
            IList<EmployeeModel> getEmpList = new List<EmployeeModel>();
            _ds = new DataSet();

            using(SqlConnection con = new SqlConnection(connect))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("EmployeeViewOrInserts", con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@mode", "GetEmpList");
                _adapter = new SqlDataAdapter(cmd);
                _adapter.Fill(_ds);
                if(_ds.Tables.Count > 0)
                {
                    for(int i=0; i<_ds.Tables[0].Rows.Count; i++)
                    {
                        EmployeeModel obj = new EmployeeModel();
                        obj.EmpID = Convert.ToInt32(_ds.Tables[0].Rows[i]["EmpID"]);
                        obj.Name = Convert.ToString(_ds.Tables[0].Rows[i]["Name"]);
                        obj.City = Convert.ToString(_ds.Tables[0].Rows[i]["City"]);
                        obj.Address = Convert.ToString(_ds.Tables[0].Rows[i]["Address"]);

                        getEmpList.Add(obj);

                    }
                }

            }


            return getEmpList;
        }


        public void InsertEmployee(EmployeeModel model)
        {
            using(SqlConnection con = new SqlConnection(connect))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("EmployeeViewOrInserts", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@mode", "AddEmployee");
                cmd.Parameters.AddWithValue("@EmpID", model.EmpID);
                cmd.Parameters.AddWithValue("@Name", model.Name);
                cmd.Parameters.AddWithValue("@City", model.City);
                cmd.Parameters.AddWithValue("@Address", model.Address);
                cmd.ExecuteNonQuery();

            }
        }


        public EmployeeModel GetEditByEmpID(int EmpID)
        {
            var model = new EmployeeModel();
            using(SqlConnection con = new SqlConnection(connect))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("EmployeeViewOrInserts", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@mode", "GetEmployeeByEmpID");
                cmd.Parameters.AddWithValue("@EmpID", EmpID);
                _adapter = new SqlDataAdapter(cmd);
                _ds = new DataSet();
                if(_ds.Tables.Count>0 && _ds.Tables[0].Rows.Count > 0)
                {
                    model.EmpID = Convert.ToInt32(_ds.Tables[0].Rows[0]["EmpID"]);
                    model.Name = Convert.ToString(_ds.Tables[0].Rows[0]["Name"]);
                    model.City = Convert.ToString(_ds.Tables[0].Rows[0]["City"]);
                    model.Address = Convert.ToString(_ds.Tables[0].Rows[0]["Address"]);
                }

            }

            return model;


        }



        public void UpdateEmp(EmployeeModel model)
        {
            using (SqlConnection con = new SqlConnection(connect))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("EmployeeViewOrInserts",con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@mode", "UpdateEmployee");
                cmd.Parameters.AddWithValue("@EmpID", model.EmpID);
                cmd.Parameters.AddWithValue("@Name", model.Name);
                cmd.Parameters.AddWithValue("@City", model.City);
                cmd.Parameters.AddWithValue("@Address", model.Address);
                cmd.ExecuteNonQuery();


            }
        }


        public void DeleteEmp(int EmpID)
        {
            using (SqlConnection con = new SqlConnection(connect))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("EmployeeViewOrInserts",con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@mode", "DeleteEmployee");
                cmd.Parameters.AddWithValue("@EmpID", EmpID);
                cmd.ExecuteNonQuery();
            }
        }


    }
}