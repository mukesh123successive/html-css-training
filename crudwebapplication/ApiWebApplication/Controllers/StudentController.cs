﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ApiWebApplication;
using crudwebapplication.Models;
using WebApiDemo1.StudentServices;

namespace ApiWebApplication.Controllers
{
    public class StudentService : ApiController
    {
        _studentService = new StudentService();
        public IHttpActionResult GetAllStudent()
       {
           return Ok(studentService.GetStudentList());
      }
       public IHttpActionResult PostStudent(EmployeeModel model)
     {
            WebApiDemo1.StudentServices.StudentService.InsertStudent(model);
          return Ok();
      }
       public IHttpActionResult PutEditSudent(int id)        {
    studentService.GetEditById(id);
                return Ok();
           }
        public IHttpActionResult PutUpadateSudent(StudentModel model)
       {
    studentService.UpdateStu(model);
                return Ok();
           }
       public IHttpActionResult DeleteStudent(int id)
       {
    studentService.DeleteStu(id);
               return Ok();
           }
}
}
    

