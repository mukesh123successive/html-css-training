﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using WebApplication4.Models;
using System.Data;
using System.Configuration;

namespace WebApplication4.Connection
{
    public class EmpConnection
    {
        
        private SqlConnection con;
           
        private void connection()
        {
            string cs = ConfigurationManager.ConnectionStrings["myconn"].ConnectionString;

            con = new SqlConnection(cs);

        }
        //To Add Employee details    
        public bool AddEmployee(EmployeeModel obj)
        {

            connection();
            SqlCommand com = new SqlCommand("AddNewEmpDetails", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@EmpID", obj.EmpID);
            com.Parameters.AddWithValue("@Name", obj.Name);
            com.Parameters.AddWithValue("@City", obj.City);
            com.Parameters.AddWithValue("@Address", obj.Address);

            con.Open();
            int i = com.ExecuteNonQuery();
            con.Close();
            if (i >= 1)
            {

                return true;

            }
            else
            {

                return false;
            }

        }

        public List<EmployeeModel> GetAllEmployees()
        {
            connection();
            List < EmployeeModel> EmpList = new List<EmployeeModel>();


            SqlCommand com = new SqlCommand("GetEmployees", con);
            com.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(com);
            DataTable dt = new DataTable();

            con.Open();
            da.Fill(dt);
            con.Close();
            //Bind EmpModel generic list using dataRow     
            foreach (DataRow dr in dt.Rows)
            {

                EmpList.Add(

                    new EmployeeModel
                    {

                        EmpID = Convert.ToInt32(dr["EmpID"]),
                        Name = Convert.ToString(dr["Name"]),
                        City = Convert.ToString(dr["City"]),
                        Address = Convert.ToString(dr["Address"])

                    }
                    );
            }

            return EmpList;
        }
        
    }
}