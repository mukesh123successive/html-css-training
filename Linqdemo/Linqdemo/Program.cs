﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Linqdemo
{
    class Program
    {
        static void Main(string[] args)
        {
           string[] names = { "mukesh", "Sanjay", "vikas", "Mohan" };

           // using query syntax
           var myLinqQuery = from name in names
                             where name.Contains('a')
                              select name;

           // craete query using method syntax
            var res = names.Where(a => a.Contains("a"));

           foreach (var name in res)
               Console.Write(name + " ");
           Console.Read();

            // Execuation of query
           // foreach (var name in myLinqQuery)
        //       // Console.Write(name + " ");
        //          Console.Read();
        }
    }
}
