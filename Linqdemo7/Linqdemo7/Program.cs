﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Linqdemo7
{
    class Program
    {
        static void Main(string[] args)
        {
            do
            {
                IEnumerable<Employee> Employees = Employee.GetAllEmployees();
                Console.Write("Please Enter Page number 1,2 or 3 \n");
                int pageNumber = 0;

                if (int.TryParse(Console.ReadLine(), out pageNumber))
                {
                    if (pageNumber >= 1 && pageNumber <= 3)
                    {
                        int pageSize = 5;
                        IEnumerable<Employee> result = Employees.Skip((pageNumber - 1) * pageSize).Take(pageSize);
                        Console.WriteLine();
                        
                        foreach (Employee emp in result)
                        {
                            Console.WriteLine(emp.EMPID + "\t" + emp.Name + "\t" + emp.Position + "\t" + emp.Salary);
                        }
                        Console.WriteLine();
                    }
                    else
                    {
                        Console.WriteLine("Page number must be integer between 1 and 3");
                    }
                }
                else
                {
                    Console.WriteLine("Page number must be integer between 1 and 3");
                }
            } while (1 == 1);
        }
    }
}
