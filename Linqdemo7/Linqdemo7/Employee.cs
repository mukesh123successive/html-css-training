﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Linqdemo7
{
    class Employee
    {
        public int EMPID { get; set; }
        public string Name { get; set; }
        public string Position { get; set; }
        public int Salary { get; set; }

        public static List<Employee> GetAllEmployees()
        {
            List<Employee> listEmployee = new List<Employee>
            {
                new Employee() {
                    EMPID=100,Name="sanjay",Position="UI Developer",Salary=50000
                },
                new Employee() {
                    EMPID=101,Name="mukesh",Position="JavaScript Developer",Salary=40000
                },
                new Employee() {
                    EMPID=102,Name="vikas",Position="Anroid Developer",Salary=45000
                },
                new Employee() {
                    EMPID=103,Name="Avneesh",Position="Windows Developer",Salary=60000
                },
                new Employee() {
                    EMPID=104,Name="Anubhav",Position="ASP.Net Developer",Salary=80000
                },
                new Employee() {
                    EMPID=105,Name="Rohit",Position="Salas Excutive",Salary=30000
                },
                new Employee() {
                    EMPID=106,Name="satyam",Position="Digital Marketing",Salary=25000
                },
                new Employee() {
                    EMPID=107,Name="Mohit",Position="Marketing",Salary=18000
                },
                new Employee() {
                    EMPID=108,Name="Pankaj",Position="Salas Excutive",Salary=35000
                },
                new Employee() {
                    EMPID=109,Name="Sangeeta",Position="UI Developer",Salary=28000
                },
                new Employee() {
                    EMPID=110,Name="Uma",Position="Salas Excutive",Salary=32000
                },
                new Employee() {
                    EMPID=111,Name="Mukesh",Position="Testing",Salary=48000
                },
                new Employee() {
                    EMPID=112,Name="Parkash",Position="HR",Salary=42000
                },
                new Employee() {
                    EMPID=113,Name="Habib",Position="Angular Developer",Salary=5800
                },
                new Employee() {
                    EMPID=114,Name="Rajesh",Position="Desginer",Salary=25000
                },
            };
            return listEmployee;
        }
    }
}
