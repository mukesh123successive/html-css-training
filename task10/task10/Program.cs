﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task10
{
    class Program
    {
        public static void method1(out int a)
        {
            a = 10;
        }
        public static void method2(ref int d)
        {
            d = 11;
        }
        public static void Main()
        {
            int b;
            int c=9;
            Program p1 = new Program();
            method1(out b);
            method2(ref c);
            Console.WriteLine("Updated value is: {0}", b);
            Console.WriteLine("Changed value is: {0}", c);
            Console.ReadKey();
        }
    }

}
    

