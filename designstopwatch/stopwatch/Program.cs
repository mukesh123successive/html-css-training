﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace stopwatch
{
    class Program
    {


        static void Main(string[] args)
        {

            string Input;
            var Time = new Stopwatch();

            Console.WriteLine("Enter S to start  and Q to stop.");
            Input = Console.ReadLine();

            while (!string.IsNullOrEmpty(Input))
            {
                if (string.Equals("s", Input) || string.Equals("S", Input))
                {
                    Time.Start();
                    Console.WriteLine("Enter Q to stop the stopwatch.");
                }
                else if (string.Equals("q", Input) || string.Equals("Q", Input))
                {
                    Time.Stop();
                    Console.WriteLine("Enter S to start the stopwatch.");
                }

                Input = Console.ReadLine();
            }
        }
    }
}
    public class Stopwatch
    {
        private Boolean _Start;
        private DateTime startTime;
        private DateTime stopTime;
        private TimeSpan timeSpan;

        public void Start()
        {
            if (_Start)
            {
                throw new InvalidOperationException();
            }

            startTime = DateTime.Now;
            _Start = true;
        Console.WriteLine("Time Now : {0}", DateTime.Now.ToString("yyyyMMddHHmmss"));
        Console.WriteLine("started at {0}", startTime.ToString("yyyyMMddHHmmss"));

    }

        public void Stop()
        {
            if (!_Start)
            {
                throw new InvalidOperationException();
            }
            stopTime = DateTime.Now;
            timeSpan = stopTime - startTime;
            _Start = false;
        Console.WriteLine("Time Now: {0}", DateTime.Now.ToString("yyyyMMddHHmmss"));
        Console.WriteLine("Stopped at {0}", stopTime.ToString("yyyyMMddHHmmss"));
        Console.WriteLine(" stopped after {0}", timeSpan);

        //return timeSpan.Seconds.ToString();
        }


    }

        
    