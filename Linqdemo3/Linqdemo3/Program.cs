﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Linqdemo3
{

    public class Employee1
    {

        public int emp_id
        {
            get;
            set;
        }

        public string emp_name
        {
            get;
            set;
        }
        public string emp_country
        {
            get;
            set;
        }
    }
    public class Employee2
    {

        public int emp_id
        {
            get;
            set;
        }

        public string emp_dept
        {
            get;
            set;
        }
        public int emp_salary
        {
            get;
            set;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            IList<int> number = new List<int>() { 10, 10, 30, 45, 50, 87 };
            IList<int> number1 = new List<int>() { 70, 10, 30, 45, 50, 90 };
            List<Employee1> emp1 = new List<Employee1>() {

            new Employee1() {emp_id = 101, emp_name = "mukesh", emp_country="india" },
                                          

            new Employee1() {emp_id = 102, emp_name = "vikas",emp_country = "Australia"},

            new Employee1() {emp_id = 103, emp_name = "Sanjay", emp_country = "india"},

            new Employee1() {emp_id = 104, emp_name = "satyam",emp_country = "india" },



            new Employee1() {emp_id = 105, emp_name = "rohan",emp_country = "india"}
                                           

        };

            List<Employee2> emp2 = new List<Employee2>() {

            new Employee2() {emp_id = 101, emp_dept = "Dotnet",emp_salary = 20000},

            new Employee2() {emp_id = 102, emp_dept = "R&D", emp_salary = 40000},

            new Employee2() {emp_id = 103, emp_dept = "HR",emp_salary = 50000},

            new Employee2() {emp_id = 104, emp_dept = "Machinelearning",emp_salary = 60000},

        };

          //query to join two list using join method
            var Result = emp1.Join(emp2,
                                e1 => e1.emp_id,
                                e2 => e2.emp_id,
                                (e1, e2) => new {
                                    EmployeeName = e1.emp_name,
                                    EmployeeDepartment = e2.emp_dept
                                });
            foreach (var result in Result)
            {
                Console.WriteLine("Employee Name: {0} Department: {1}",
                             result.EmployeeName, result.EmployeeDepartment);
            }


            //use of select operator

            Console.WriteLine("employee name and id");

            var res = emp1.Select(a => new { ID = a.emp_id, Name = a.emp_name });

            foreach (var val in res)
            {
                Console.WriteLine("Employee Name : {0}  Employee ID : {1}",
                                                         val.Name, val.ID);
            }

            //use of all operator
            bool ids = emp1.All(s => s.emp_id > 101 && s.emp_id < 104);
            Console.WriteLine(ids);

            //use of element or elementat operator
            Console.WriteLine("1st Element in intList: {0}", number.ElementAt(0));
            Console.WriteLine("1st Element in intList: {0}", number.ElementAtOrDefault(6));


            //use of distinct,except and intersect operator
            //  var distinctresult = number.Distinct();

            var exceptresult = number.Except(number1);

            foreach (var r in exceptresult)
                Console.WriteLine(r);

            Console.ReadKey();
        }
    }
}
